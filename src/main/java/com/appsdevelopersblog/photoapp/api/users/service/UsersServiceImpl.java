package com.appsdevelopersblog.photoapp.api.users.service;

import com.appsdevelopersblog.photoapp.api.users.data.AlbumsServiceClient;
import com.appsdevelopersblog.photoapp.api.users.data.UserEntity;
import com.appsdevelopersblog.photoapp.api.users.data.UsersRepository;
import com.appsdevelopersblog.photoapp.api.users.shared.UserDto;
import com.appsdevelopersblog.photoapp.api.users.ui.model.AlbumResponseModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UsersServiceImpl implements UserService {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);

    @Autowired
    private UsersRepository repository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    AlbumsServiceClient albumsServiceClient;
    //private RestTemplate restTemplate;

    @Autowired
    private Environment environment;

    @Override
    public UserDto createUser(UserDto userDetails) {
        userDetails.setUserId(UUID.randomUUID().toString());
        userDetails.setEncryptedPassword(bCryptPasswordEncoder.encode(userDetails.getPassword()));

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        UserEntity userEntity = modelMapper.map(userDetails, UserEntity.class);
        repository.save(userEntity);

        UserDto returnValue = modelMapper.map(userEntity, UserDto.class);

        return returnValue;
    }

    @Override
    public UserDto getUserDetailsByEmail(String email) throws UsernameNotFoundException {
        LOGGER.info("INICIANDO GETUSERBYEMAIL: " + email);
        UserEntity userEntity = repository.findByEmail(email);

        LOGGER.info(userEntity.toString());

        if (userEntity == null) throw new UsernameNotFoundException(email);

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper.map(userEntity, UserDto.class);
    }

    @Override
    public UserDto getUserByUserId(String userId) {
        UserEntity userEntity = repository.findByUserId(userId);
        if (userEntity == null) throw new UsernameNotFoundException("User not found");

        UserDto userDto = new ModelMapper().map(userEntity, UserDto.class);

//        String albumsUrl = String.format(environment.getProperty("albums.url"), userId);
//        ResponseEntity<List<AlbumResponseModel>> albumsListResponse = restTemplate.exchange(albumsUrl, HttpMethod.GET,
//                null, new ParameterizedTypeReference<List<AlbumResponseModel>>() {});
//        List<AlbumResponseModel> albumsList = albumsListResponse.getBody();

        LOGGER.info("Before calling albums microservice");
        List<AlbumResponseModel> albumsList = albumsServiceClient.getAlbums(userId);
        LOGGER.info("After calling albums microservice");
        userDto.setAlbums(albumsList);

        return userDto;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = repository.findByEmail(username);
        LOGGER.info("INICIANDO loadUserByUsername: " + userEntity);

        if (userEntity == null) throw new UsernameNotFoundException(username);

        return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), true, true,
                true, true, new ArrayList<>());
    }


}
