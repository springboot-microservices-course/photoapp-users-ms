package com.appsdevelopersblog.photoapp.api.users.shared;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class FeignErrorDecoder implements ErrorDecoder {

    @Autowired
    private Environment environment;

    @Override
    public Exception decode(String s, Response response) {
        switch (response.status()) {
            case 400:
                // return new BasdRequestException();
                break;
            case 404:
                if (s.contains("getAlbums")) {
                    System.out.println(environment.getProperty("albums.exceptions.albums-not-found"));
                    return new ResponseStatusException(HttpStatus.valueOf(response.status()),  environment.getProperty("albums.exceptions.albums-not-found"));
                }
                break;
            default:
                return new Exception(response.reason());
        }
        return null;
    }
}
