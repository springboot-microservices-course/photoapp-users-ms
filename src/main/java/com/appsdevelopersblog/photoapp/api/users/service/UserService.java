package com.appsdevelopersblog.photoapp.api.users.service;

import com.appsdevelopersblog.photoapp.api.users.shared.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService extends UserDetailsService {
    UserDto createUser(UserDto userDetails);
    UserDto getUserDetailsByEmail(String email) throws UsernameNotFoundException;
    UserDto getUserByUserId(String userId);
}
